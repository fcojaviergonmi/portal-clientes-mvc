<?php
/**
 * Created by JetBrains PhpStorm.
 * User: franciscogonzalez
 * Date: 1/18/13
 * Time: 12:42 PM
 * To change this template use File | Settings | File Templates.
 */
class usuarioModel extends Model
{
    public function __construct()
    {
        parent::__construct();
    }
    public function getUsuarioById($id)
    {
        $usuario = $this->_db->query(
            "select * from " . DB_PREFIX . "usuario where id = $id"
        );
        return $usuario->fetch();
    }
    public function verificarUsuario($usuario)
    {
        $id = $this->_db->query(
            "select id from " . DB_PREFIX . "usuario  where usuario = '$usuario'"
        );

        return $id->fetch();
    }
    public function verificarEmail($email)
    {
        $id = $this->_db->query(
            "select id from " . DB_PREFIX . "usuario where email = '$email'"
        );
        if($id->fetch()){
            return true;
        }
        return false;
    }
    public function registrarUsuario($nombre, $usuario, $password, $email, $rol,$logo,$ve_informe,$ve_paleta,$ve_orden,$ve_precio)
    {
        $reg = $this->_db->prepare("insert into " . DB_PREFIX . "usuario  values (null, :nombre, :usuario ,:pass, :email, :rol, 1, now(), :logo, :ve_precio, :ve_informe, :ve_paleta, :ve_orden)")
            ->execute(array(
            ':nombre' => $nombre,
            ':usuario' => $usuario,
            ':pass' => Hash::getHash('sha1', $password, HASH_KEY),
            ':email' => $email,
            ':rol' => $rol,
            ':logo' => $logo,
            ':ve_precio' => $ve_precio,
            ':ve_informe' => $ve_informe,
            ':ve_paleta' => $ve_paleta,
            ':ve_orden' => $ve_orden,
                
        ));
        if($reg){
            return true;
        }
    }
    public function editarUsuario($id,$nombre, $usuario, $email,$rol,$logo,$ve_informe,$ve_paleta,$ve_orden,$ve_precio)
    {
        $id = (int) $id;
        $estado=$this->_db->prepare("UPDATE " . DB_PREFIX . "usuario SET  nombre = :nombre,usuario = :usuario,
        email = :email,role = :role ,
        logo = :logo,ve_informe = :ve_informe,ve_paleta = :ve_paleta,
        ve_orden = :ve_orden,ve_precio = :ve_precio
        WHERE id = :id")
            ->execute(
                array(
                    ':id' => $id,
                    ':nombre' => $nombre,
                    ':usuario' => $usuario,
                    ':email' => $email,
                    ':role' => $rol,
                    ':logo' => $logo,
                    ':ve_informe' => $ve_informe,
                    ':ve_paleta' => $ve_paleta,
                    ':ve_orden' => $ve_orden,
                    ':ve_precio' => $ve_precio,

                ));

        if($estado){
            return true;
        }
    }
    public function getUsuarios()
    {
        $id = $this->_db->query("select * from " . DB_PREFIX . "usuario WHERE role=1 order by id DESC");
        return $id->fetchall();
    }
    public function getUsuarioRuts($id)
    {
        $rut = $this->_db->query("select * from " . DB_PREFIX . "usuario_rut r INNER JOIN " . DB_PREFIX . "rut ru ON ru.`id`=r.`id_rut` WHERE r.`id_usuario`='$id' group by rut order by id DESC");
        return $rut->fetchall();
    }
    public function getUsuarioByIdRut($id)
    {
        $rut = $this->_db->query("select * from " . DB_PREFIX . "usuario_rut r INNER JOIN " . DB_PREFIX . "usuario ru ON ru.`id`=r.`id_usuario` WHERE r.`id_rut`='$id' group by id order by id DESC");
        return $rut->fetchall();
    }
    public function getUsuarioRutPorDefecto($id)
    {
        $rut = $this->_db->query("select * from " . DB_PREFIX . "usuario_rut r INNER JOIN " . DB_PREFIX . "rut ru ON ru.`id`=r.`id_rut` WHERE r.`id_usuario`='$id' AND r.por_defecto=1 group by rut order by id DESC");
        return $rut->fetch();
    }

    public function editarUsuarioPass($id,$password)
    {
        $id = (int) $id;
        $this->_db->prepare("UPDATE " . DB_PREFIX . "usuario SET pass = :pass WHERE id = :id")
            ->execute(
                array(
                    ':id' => $id,
                    ':pass' => Hash::getHash('sha1', $password, HASH_KEY)
                ));
    }
    public function eliminarUsuario($id)
    {
        $id = (int) $id;
        $this->_db->query("DELETE FROM " . DB_PREFIX . "usuario WHERE id = $id AND role=1");
        $this->_db->query("DELETE FROM " . DB_PREFIX . "usuario_rut WHERE id_usuario = '$id'");
    }
}