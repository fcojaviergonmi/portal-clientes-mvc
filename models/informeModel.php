<?php
/*

*/
class informeModel extends Model
{
    public function __construct()
    {
        parent::__construct();
    }
    public function getProductosMasConsumidosMulti($rut, $fechaP)
    {

        $qfecha="";
        $qRut="";
            if(count($fechaP)>1){

            $count=0;
            foreach($fechaP as $fecha){
                if($count==0){
                    $qfecha .= " AND ( c.feemdo like '%" . $fecha . "%'";

                }else{
                    $qfecha .= " OR c.feemdo like '%" . $fecha . "%'";
                }
                $count++;
            }

                $qfecha .= ")";

            }else{

                $qfecha .= " AND c.feemdo like '%" . $fechaP[0] . "%'";
            }

            if(count($rut)>1){

                $qRut .= 'c.endo IN ('.implode(',', $rut).')';


            }else{

                $qRut .= "c.endo='$rut[0]'";
            }

        $data = $this->_db->query("SELECT  FORMAT(SUM(REPLACE(c.vaneli,',','')),0) as vaneli,c.`nokofm` from " . DB_PREFIX . "cliente c where " . $qRut . "  ".$qfecha." group by c.`nokofm`");

        return $data->fetchall();
    }
    public function getProductosMasConsumidos($rut, $fecha)
    {

        $data = $this->_db->query("SELECT  FORMAT(SUM(REPLACE(c.vaneli,',','')),0) as vaneli,c.`nokofm` from " . DB_PREFIX . "cliente c where endo=" . $rut . " AND c.feemdo like '%" . $fecha . "%' group by c.`nokofm`");

        return $data->fetchall();
    }

    public function getDatosUsuario($id)
    {


        $data = $this->_db->query("select * from ".DB_PREFIX."usuario " .
                "where id = '$id' ");
        return $data->fetch();
    }
    public function getComunasProductos($rut, $fechaP)
    {

        $qfecha="";
        $qRut="";

        $qfecha .= " AND c.feemdo like '%" . $fechaP . "%'";
        $qRut .= "endo='$rut'";

        $data = $this->_db->query("SELECT c.`nokosu` from " . DB_PREFIX . "cliente c WHERE " . $qRut . "   ".$qfecha."  group by c.`nokosu` order by c.`nokosu` DESC");
        return $data->fetchall();
    }
    public function getComunasProductosMulti($rut, $fechaP)
    {

        $qfecha="";
        $qRut="";
        if(count($fechaP)>1){

            $count=0;
            foreach($fechaP as $fecha){
                if($count==0){
                    $qfecha .= " AND ( c.feemdo like '%" . $fecha . "%'";

                }else{
                    $qfecha .= " OR c.feemdo like '%" . $fecha . "%'";
                }
                $count++;
            }

            $qfecha .= ")";

        }else{

            $qfecha .= " AND c.feemdo like '%" . $fechaP[0] . "%'";
        }

        if(count($rut)>1){
            $qRut .= 'c.endo IN ('.implode(',', $rut).')';
        }else{

            $qRut .= "endo='$rut[0]'";
        }


        $data = $this->_db->query("SELECT c.`nokosu` from " . DB_PREFIX . "cliente c WHERE " . $qRut . "   ".$qfecha."  group by c.`nokosu` order by c.`nokosu` DESC");

        return $data->fetchall();
    }
    function getListFechasInformes($rut)
    {
        $data = $this->_db->query("SELECT DATE_FORMAT(o.feemdo, '%Y-%m') as fecha from " . DB_PREFIX . "cliente o  WHERE o.endo = '$rut' group by DATE_FORMAT(o.feemdo, '%Y-%m') order by fecha DESC ");

        return $data->fetchall();
    }
    public function getProductosMasConsumidosPorSucursal($rut , $fechaP)
    {
        try {
            $this->_db->beginTransaction();
            $query = "SET @sql = NULL;";
            $stm = $this->_db->prepare($query);
            $stm->execute();
            $query = "SET group_concat_max_len=4096;";
            $stm = $this->_db->prepare($query);
            $stm->execute();
            $query = "SELECT GROUP_CONCAT(DISTINCT CONCAT('FORMAT(SUM(case when nokosu = ''',nokosu,'''";
            $query .= 'then REPLACE(vaneli,",","") end) ,0) AS ';
            $query .= "''',nokosu, '''')) INTO @sql FROM " . DB_PREFIX . "cliente WHERE endo=".$rut." AND feemdo like '%".$fechaP ."%';";
            $stm = $this->_db->prepare($query);
            $stm->execute();
            $query = "SET @sql = CONCAT('SELECT nokofm, ', @sql, ' FROM " . DB_PREFIX . "cliente WHERE endo=".$rut;
            $query .= ' AND feemdo like "%'.$fechaP.'%"';
            $query .= " GROUP BY nokofm');";
            $stm = $this->_db->prepare($query);
            $stm->execute();
            $query = "PREPARE stmt FROM @sql;";
            $stm = $this->_db->prepare($query);
            $stm->execute();
            $query = "EXECUTE stmt;";
            $stm = $this->_db->prepare($query);
            $stm->execute();
            $data = $stm->fetchAll();
            $this->_db->commit();

            return $data;
        } catch (PDOException $ex) {
            //Something went wrong rollback!
            $this->_db->rollBack();
            echo $ex->getMessage();
        }

    }
    public function getProductosMasConsumidosPorSucursalMulti($rut , $fechaP)
    {


        $qfecha="";
        $qRut="";
        if(count($fechaP)>1){

            $count=0;
            foreach($fechaP as $fecha){
                if($count==0){
                    $qfecha .= ' AND ( feemdo like "%' . $fecha . '%"';

                }else{
                    $qfecha .= ' OR feemdo like "%' . $fecha . '%"';
                }
                $count++;
            }

            $qfecha .= ")";
        }else{

            $qfecha .= ' AND feemdo like "%'. $fechaP[0] . '%"';
        }

        if(count($rut)>1){
            $qRut .= "endo IN (".implode(",", $rut).")";
        }else{

            $qRut .= "endo=".$rut[0];
        }

        try {
            $this->_db->beginTransaction();
            $query = "SET @sql = NULL;";
            $stm = $this->_db->prepare($query);
            $stm->execute();
            $query = "SET group_concat_max_len=4096;";
            $stm = $this->_db->prepare($query);
            $stm->execute();
            $query = "SELECT GROUP_CONCAT(DISTINCT CONCAT('FORMAT(SUM(case when nokosu = ''',nokosu,'''";
            $query .= 'then REPLACE(vaneli,",","") end) ,0) AS ';
            $query .= "''',nokosu, '''')) INTO @sql FROM " . DB_PREFIX . "cliente WHERE ".$qRut." $qfecha ;";
            $stm = $this->_db->prepare($query);
            $stm->execute();
            $query = "SET @sql = CONCAT('SELECT nokofm, ', @sql, ' FROM " . DB_PREFIX . "cliente WHERE ".$qRut;
            $query .= ' '.$qfecha;
            $query .= " GROUP BY nokofm');";
            $stm = $this->_db->prepare($query);
            $stm->execute();
            $query = "PREPARE stmt FROM @sql;";
            $stm = $this->_db->prepare($query);
            $stm->execute();
            $query = "EXECUTE stmt;";
            $stm = $this->_db->prepare($query);
            $stm->execute();
            $data = $stm->fetchAll();
            $this->_db->commit();

            return $data;
        } catch (PDOException $ex) {
            //Something went wrong rollback!
            $this->_db->rollBack();
            echo $ex->getMessage();
        }

    }
}
?>