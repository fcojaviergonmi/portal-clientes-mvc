<?php
/**
 * Created by JetBrains PhpStorm.
 * User: franciscogonzalez
 * Date: 6/20/13
 * Time: 6:15 PM
 * To change this template use File | Settings | File Templates.
 */
class cargarModel extends Model
{
    public function __construct()
    {
        parent::__construct();
    }
    public function insertInforme($sql)
    {
    if($this->_db->query("TRUNCATE " . DB_PREFIX . "cliente_temp")){
        if($this->_db->query($sql)){
            return true;
        }
      }
    }
    public function insertInformeOrdenes($sql)
    {

            if($this->_db->query($sql)){
                return true;
            }

    }

    public function insertInformePaleta($sql)
    {

        if($this->_db->query($sql)){
            return true;
        }

    }
    public function updateInforme()
    {
        if($this->_db->query("INSERT INTO " . DB_PREFIX . "cliente (`endo`, `nokosu`, `feemdo`, `vaneli`, `nokofm`,`fecha_carga`) SELECT it.`endo`,it.`nokosu`,it.`feemdo`,sum(it.`vaneli`),it.`nokofm`,it.`fecha_carga` FROM " . DB_PREFIX . "cliente_temp it GROUP BY it.`endo`,it.`nokosu`,it.`nokofm`,DATE_FORMAT(it.`feemdo`, '%Y-%m'),it.`fecha_carga`")){
                return true;
        }
    }
    public function insertRegistroHistorico($id,$fecha_carga,$inputFileRealName)
    {
        if($this->_db->query("INSERT INTO  " . DB_PREFIX . "historico_cargas  (`tipo`, `fecha_carga`, `nombre_archivo`) values ('$id','$fecha_carga','$inputFileRealName')")){
            return true;
        }
    }

    public function getCargasHistoricas($tipo)
    {
        if($data=$this->_db->query("SELECT * FROM " . DB_PREFIX . "historico_cargas WHERE tipo=$tipo")){
           return $data->fetchAll();
        }
    }

    public function eliminarCarga($id)
    {
        $id = (int) $id;
        $data = $this->_db->query("SELECT * FROM " . DB_PREFIX . "historico_cargas WHERE id = $id AND tipo=1");
        $registro=$data->fetch();
        $borrado = $this->_db->query("DELETE FROM " . DB_PREFIX . "cliente WHERE fecha_carga = '".$registro['fecha_carga']."'");
        if($borrado){
            $borrar_historico= $this->_db->query("DELETE FROM " . DB_PREFIX . "historico_cargas WHERE id = $id");
            if($borrar_historico){
                return true;
            }
        }

    }

    public function eliminarCargaOrdenes($id)
    {
        $id = (int) $id;
        $data = $this->_db->query("SELECT * FROM " . DB_PREFIX . "historico_cargas WHERE id = $id and tipo=2");
        $registro=$data->fetch();
        $borrado = $this->_db->query("DELETE FROM " . DB_PREFIX . "orden WHERE fecha_carga = '".$registro['fecha_carga']."'");
        if($borrado){
            $borrar_historico= $this->_db->query("DELETE FROM " . DB_PREFIX . "historico_cargas WHERE id = $id");
            if($borrar_historico){
                return true;
            }
        }

    }
    public function eliminarCargaPaleta($id)
    {
        $id = (int) $id;
        $data = $this->_db->query("SELECT * FROM " . DB_PREFIX . "historico_cargas WHERE id = $id and tipo=3");
        $registro=$data->fetch();
        $borrado = $this->_db->query("DELETE FROM " . DB_PREFIX . "paleta WHERE fecha_carga = '".$registro['fecha_carga']."'");
        if($borrado){
            $borrar_historico= $this->_db->query("DELETE FROM " . DB_PREFIX . "historico_cargas WHERE id = $id");
            if($borrar_historico){
                return true;
            }
        }

    }


}