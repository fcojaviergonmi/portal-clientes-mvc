<?php
/**
 * Created by JetBrains PhpStorm.
 * User: franciscogonzalez
 * Date: 6/20/13
 * Time: 6:15 PM
 * To change this template use File | Settings | File Templates.
 */

class fechaModel extends Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getFechas()
    {
        $id = $this->_db->query("select * from " . DB_PREFIX . "fecha order by fecha DESC");
        return $id->fetchall();
    }

    public function registrarFecha($anho,$mes)
    {
        $reg = $this->_db->prepare(
            "insert into " . DB_PREFIX . "fecha  values" .
            "(:fecha)"
        )
            ->execute(array(
                ':fecha' => $anho."-".$mes
            ));
        if($reg){
            return true;
        }
    }

    public function validarFecha($anho,$mes)
    {
        $val = $this->_db->query("select * from " . DB_PREFIX . "fecha WHERE fecha='$anho-$mes'");
        if($val->fetch()){
            return true;
        }
        return false;
    }

    public function eliminarFecha($fecha)
    {
        $this->_db->query("DELETE FROM " . DB_PREFIX . "fecha WHERE fecha = '$fecha'");
    }
}