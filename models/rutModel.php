<?php
/**
 * Created by JetBrains PhpStorm.
 * User: franciscogonzalez
 * Date: 6/20/13
 * Time: 6:15 PM
 * To change this template use File | Settings | File Templates.
 */

class rutModel extends Model
{
    public function __construct()
    {
        parent::__construct();
    }
    public function verificarRut($rut)
    {
        $id = $this->_db->query(
            "select id from " . DB_PREFIX . "rut  where rut = '$rut'"
        );
        return $id->fetch();
    }
    public function registrarRut($rut)
    {
        $reg = $this->_db->prepare(
            "insert into " . DB_PREFIX . "rut  values" .
            "(null,:rut)"
        )
            ->execute(array(
                ':rut' => $rut,
            ));
        if($reg){
            return true;
        }
    }
    public function agregarUsuario($id,$id_usuario)
    {
        $reg = $this->_db->prepare("insert into " . DB_PREFIX . "usuario_rut (id_usuario,id_rut) values (:id_usuario,:id_rut)")
            ->execute(array(
                ':id_usuario' => $id_usuario,
                ':id_rut' => $id
            ));
        if($reg){
            return true;
        }
    }
    public function validarUsuario($id,$id_usuario)
    {
        $val = $this->_db->query("select * from " . DB_PREFIX . "usuario_rut WHERE id_rut=$id AND id_usuario=$id_usuario");

        if($val->fetch()){
            return true;
        }
        return false;
    }

    public function getRUTS()
    {
        $id = $this->_db->query("select * from " . DB_PREFIX . "rut order by id DESC");
        return $id->fetchall();
    }
    public function getRutById($id)
    {
        $usuario = $this->_db->query(
            "select * from " . DB_PREFIX . "rut where id = $id"
        );
        return $usuario->fetch();
    }
    public function eliminarRut($id)
    {
        $id = (int) $id;
        $this->_db->query("DELETE FROM " . DB_PREFIX . "rut WHERE id = $id");
        $this->_db->query("DELETE FROM " . DB_PREFIX . "usuario_rut WHERE id_rut = $id");
    }
    public function eliminarUsuarioById($id,$id_usuario)
    {
        $id = (int) $id;
        $id_usuario = (int) $id_usuario;
        $this->_db->query("DELETE FROM " . DB_PREFIX . "usuario_rut WHERE id_usuario = $id_usuario AND id_rut=$id");
    }
    public function getUsuarioDisponibles($id)
    {
        $id = (int) $id;
        $usuario=$this->_db->query("SELECT ir.`id`,ir.`nombre`,ir.usuario FROM " . DB_PREFIX . "usuario ir WHERE ir.`id` NOT IN (SELECT u.`id_usuario` FROM " . DB_PREFIX . "usuario_rut u WHERE u.`id_rut`=$id) AND ir.`role`=1");
        return $usuario->fetchall();

    }
}