<?php
/*
 *
 *
 */
class productoModel extends Model
{
    public function __construct()
    {
        parent::__construct();
    }
    public function getArbolCategorias($rut)
    {
        $data = $this->_db->query("SELECT nokofm FROM ".DB_PREFIX."paleta WHERE rut=".$rut." GROUP BY nokofm ORDER BY nokofm ASC");
        return $data->fetchall();
    }
    public function getProductSkuByCategory($rut,$cat)
    {
        
        $productosFinal="";
        $productos2=false;

        if($data = $this->_db->query("SELECT `codigo_garmendia` FROM ".DB_PREFIX."paleta WHERE rut=".$rut." AND nokofm='".$cat."' GROUP BY codigo_garmendia ORDER BY nokofm ASC")){

           // echo "SELECT `codigo_garmendia` FROM ".DB_PREFIX."paleta WHERE rut=".$rut." AND nokofm='".$cat."' GROUP BY codigo_garmendia ORDER BY nokofm ASC";
            if($productos=$data->fetchall()){
                foreach($productos as $producto){
                    $productosFinal.=$producto['codigo_garmendia'].",";
                }
            }

            $productosFinal=substr($productosFinal, 0, -1);
			
            if(!empty($productosFinal)){

                $combinaciones="";

                if($dataCombinaciones = $this->_db->query("select GROUP_CONCAT(distinct(pl.id_product)) as combinaciones from `gmps_product_attribute` pl
                                                WHERE pl.reference IN (".$productosFinal.") ORDER BY pl.id_product ASC")){

                    $combinaciones=$dataCombinaciones->fetchall();
                    
                }
                $productosSin="";

                if($productosSincombinacion = $this->_db->query("select GROUP_CONCAT(distinct(pl.id_product)) as productoSin from `gmps_product` pl
                                                WHERE pl.reference IN (".$productosFinal.") ORDER BY pl.id_product ASC")){
                    $productosSin=$productosSincombinacion->fetchall();

                }


                
                if(empty($combinaciones[0]['combinaciones'])){
                    $combinaciones[0]['combinaciones']="";
                }else{
					$combinaciones[0]['combinaciones']=",".$combinaciones[0]['combinaciones'];
				}

                if(empty($productosSin[0]['productoSin'])){
                    $productosSin[0]['productoSin']="";
                    $combinaciones[0]['combinaciones']=substr($combinaciones[0]['combinaciones'],1);
                }else{
                    $productosSin[0]['productoSin']=" ".$productosSin[0]['productoSin'];
                }


                if($data2 = $this->_db->query("SELECT DISTINCT p.id_product, pl.name, i.id_image,p.reference
                                                FROM gmps_product p 
                                                inner join gmps_product_lang pl on pl.id_product=p.id_product 
                                                left join gmps_image i on i.`id_product`=p.id_product 
                                                WHERE p.`id_product` IN (". $productosSin[0]['productoSin'].$combinaciones[0]['combinaciones'].")
                                                group by p.id_product
                                                ORDER BY pl.id_product ASC")){


                    $productos2=$data2->fetchall();


                }
                

                if(is_array($productos2)){
                    foreach($productos2 as $keyProd2=>$producto2){
                        $urlImage="img/p/";
                        $productos2[$keyProd2]['url_image']="";
                        $urlImageResp=$urlImage;
                        if(!empty($productos2[$keyProd2]['id_image'])){

                            for($i=0;(isset($productos2[$keyProd2]['id_image']{$i}));$i++){
                                $urlImage.=$productos2[$keyProd2]['id_image']{$i}."/";
                            }
                            if(file_exists($_SERVER['DOCUMENT_ROOT']."/catalogo/".$urlImage.$productos2[$keyProd2]['id_image']."-medium_default.jpg")){
                                $productos2[$keyProd2]['url_image']=$urlImage.$productos2[$keyProd2]['id_image']."-medium_default.jpg";
                            }
                        }else{
                            $urlImage=$urlImageResp;
                            $productos2[$keyProd2]['url_image']=$urlImage."es-default-medium_default.jpg";
                        }
                    }
                }
            }
        }

        return $productos2;
    }
    public function getProduct($id,$rut){
        
        $resp=false;
        
        $id=str_replace("'","\'",$id);
        
        if($data = $this->_db->query("SELECT cp.name as catName, pl.id_product, p.reference, pl.description, pl.name, i.id_image
                                        FROM gmps_product p 
                                        inner join gmps_product_lang pl on pl.id_product=p.id_product 
                                        left join gmps_image i on i.`id_product`=p.id_product
                                        inner join gmps_category_lang cp on cp.`id_category`=p.`id_category_default`
                                        WHERE p.id_product ='".$id."' AND pl.id_lang='4' and cp.id_lang='4' ORDER BY p.id_product ASC")){


            $resp['producto']=$data->fetchAll();

           // echo "SELECT `precio` FROM ".DB_PREFIX."paleta WHERE rut=".$rut." AND codigo_garmendia='".$resp['producto'][0]['reference']."' ";
            $data2 = $this->_db->query("SELECT `precio` FROM ".DB_PREFIX."paleta WHERE rut=".$rut." AND codigo_garmendia='".$resp['producto'][0]['reference']."' ");

            $resp['precio'] = $data2->fetch();


        }
        
        //Aplicar url de img
        foreach($resp['producto'] as $keyProd2=>$producto2){
            $urlImage="img/p/";
            $resp['producto'][$keyProd2]['url_image']="";
            $urlImageResp=$urlImage;
            
            if(!empty($resp['producto'][$keyProd2]['id_image'])){
                for($i=0;(isset($resp['producto'][$keyProd2]['id_image']{$i}));$i++){
                    $urlImage.=$resp['producto'][$keyProd2]['id_image']{$i}."/";
                }

                if(file_exists($_SERVER['DOCUMENT_ROOT']."/catalogo/".$urlImage.$resp['producto'][$keyProd2]['id_image']."-large_default.jpg")){
                    $resp['producto'][$keyProd2]['url_image']=$urlImage.$resp['producto'][$keyProd2]['id_image']."-large_default.jpg";
                }
            }else{
                $urlImage=$urlImageResp;
                $resp['producto'][$keyProd2]['url_image']=$urlImage."es-default-large_default.jpg";
            }
        }
        if($data2 = $this->_db->query("select fl.name, fvl.value
                                        from gmps_feature_value_lang fvl
                                        inner join gmps_feature_product fp on fp.id_feature_value=fvl.`id_feature_value`
                                        inner join gmps_feature_lang fl on fl.`id_feature`=fp.`id_feature`
                                        where fvl.id_lang='4'and fl.id_lang='4' and fp.id_product='".$id."'")){
            $resp['caracteristicas']=$data2->fetchall();
        }
        return $resp;
    }
}
?>