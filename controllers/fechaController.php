<?php
/**
 * Created by JetBrains PhpStorm.
 * User: franciscogonzalez
 * Date: 6/20/13
 * Time: 6:16 PM
 * To change this template use File | Settings | File Templates.
 */

class fechaController extends Controller
{

    private $_usuario;
    private $_fecha;

    public function __construct()
    {
        parent::__construct();
        $this->_usuario = $this->loadModel('usuario');
        $this->_fecha = $this->loadModel('fecha');
    }

    public function index()
    {
        if (!Session::get('autenticado')) {
            $this->redireccionar('error');
        }
        Session::acceso('admin');
        $this->_view->titulo = 'ADMINISTRADOR DE FECHAS';
        $this->_view->thispage = 'admfecha';
        $this->_view->usuario = $this->_usuario->getUsuarioById(Session::get('id_usuario'));
        $this->_view->renderizar('index');
    }

    public  function crear()
    {
        if (!Session::get('autenticado')) {
            $this->redireccionar('error');
        }
        Session::acceso('admin');

        $this->_view->thispage = 'admfecha';
        $this->_view->titulo = 'CREAR FECHA';
        $this->_view->usuario = $this->_usuario->getUsuarioById(Session::get('id_usuario'));

        if($this->getInt('enviar') == 2){


            if(!$this->getSql('anho')){
                $this->_view->_error = 'Debe ingresar un año';
                $this->_view->renderizar('crear');
                exit;
            }
            if(!$this->getSql('mes')){
                $this->_view->_error = 'Debe ingresar un mes';
                $this->_view->renderizar('crear');
                exit;
            }

            $validar = $this->_fecha->validarFecha($this->getSql('anho'),$this->getSql('mes'));

            if(!$validar){

                $registro = $this->_fecha->registrarFecha(
                    $this->getSql('anho'),
                    $this->getSql('mes')
                );

                if($registro){

                    $this->_view->datos = false;
                    $this->_view->_mensaje = 'Registro Completado';
                }else{
                    $this->_view->datos = false;
                    $this->_view->_mensaje = 'Registro Error';
                }

            }else{

                $this->_view->datos = false;
                $this->_view->_mensaje = 'Regristo Duplicado';
            }



        }


        $this->_view->renderizar('crear');

    }
    public function eliminar($fecha)
    {
        if(!Session::get('autenticado')){
            $this->redireccionar('error');
        }
        Session::acceso('admin');

        $this->_fecha->eliminarFecha($fecha);
        $this->redireccionar('fecha/lista');
    }

    public function lista($pagina = false)
    {
        if(!Session::get('autenticado')){
            $this->redireccionar('error');
        }
        $this->_view->thispage = 'admfecha';
        $this->_view->titulo = 'LISTA DE FECHAS';
        $this->_view->usuario = $this->_usuario->getUsuarioById(Session::get('id_usuario'));

        if(!$this->filtrarInt($pagina)){
            $pagina = false;
        }
        else{
            $pagina = (int) $pagina;
        }
        $this->getLibrary('paginador');
        $paginador = new Paginador();


        $this->_view->fechas = $paginador->paginar($this->_fecha->getFechas(), $pagina);
        $this->_view->paginacion = $paginador->getViewAdmin('paginador', 'fecha/lista');

        $this->_view->renderizar('lista');
    }

}