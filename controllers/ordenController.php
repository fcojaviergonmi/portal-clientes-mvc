<?php
/*
 * Created by JetBrains PhpStorm.
 * User: franciscogonzalez
 * Date: 11/23/12
 * Time: 11:29 AM
 * To change this template use File | Settings | File Templates.
 *
 *
*/

class ordenController extends Controller
{

    private $_orden;
    private $_orden_usuario;
    private $_fecha;

    public function __construct() {
        parent::__construct();
        $this->_orden = $this->loadModel('orden');
        $this->_orden_usuario = $this->loadModel('usuario');

        $this->_fecha = $this->loadModel('fecha');
    }

    private function validarRut($rut){

        $respuesta=0;

        foreach ($this->_view->rut_usuario as $ruLista){
            if($rut==$ruLista['rut']){
                $respuesta = 1;
                break;
            }else{ }
        }
        return $respuesta;
    }

    public function index($pagina = false)
    {

        if(!Session::get('autenticado')){
            $this->redireccionar('index');
        }
        Session::acceso('usuario');
        /* inicio estado variables*/
        $fecha=false;
        $rut=false;
        $noRut=false;
        $this->_view->avanzado=false;
        $this->_view->simple=true;
        $this->_view->tiene_fecha=true;
        $this->_view->tiene_rut=true;
        $this->_view->fecha=false;
        $this->_view->rut=false;

        $this->_view->thispage = 'status';
        /* obtengo data de usuario*/
        $this->_view->usuario = $this->_orden_usuario->getUsuarioById(Session::get('id_usuario'));
        if($this->_view->usuario['ve_orden']){
        /* FECHAS y RUTS */
        $this->_view->rut_usuario = $this->_orden_usuario->getUsuarioRuts($this->_view->usuario['id']);
        $this->_view->rut_usuario_por_defecto = $this->_orden_usuario->getUsuarioRutPorDefecto($this->_view->usuario['id']);
        $this->_view->fechas_informe = $this->_fecha->getFechas();

        /* OBTENGO ULTIMA DATA SIN PETICION POST */
        if(!$this->getInt('enviar')){
            if(!empty($this->_view->fechas_informe)){
                $fecha = $this->_view->fechas_informe[count($this->_view->fechas_informe)-1]['fecha'];
                $this->_view->fecha[]=$fecha;
            }else{ }
            if(!empty($this->_view->rut_usuario_por_defecto)){
                $rut = $this->_view->rut_usuario_por_defecto['rut'];
                $this->_view->rut[]=$rut;
            }else{ }
        }
        /*Peticion Simple POST*/
        if($this->getInt('enviar') == 1){
            $this->_view->datos = $_POST;
            if(!empty($_POST['fecha'])){
                $fecha = $_POST['fecha'];
                $this->_view->fecha[]=$fecha;
            }else{ }
            if(!empty($_POST['rut'])){
                $rut = $_POST['rut'];
                $this->_view->rut[]=$rut;
            }else{ }

        }

        if(count($rut)>1){
            foreach ($rut as $ru){
                $estado = $this->validarRut($ru);
                if($estado==0){
                    $noRut=true;
                }
            }
        }else{
            $estado = $this->validarRut($rut);
            if($estado==0){
                $noRut=true;
            }
        }
        if (!$noRut) {

        if(!empty($fecha) &&!empty($rut)){
        $this->_view->ordenes = $this->_orden->getOrdenesByEndoYFecha($rut,$fecha);
        }

        }
        }else{
            $this->_view->mensaje_permisos="No tiene permisos para ver ordenes";
        }
        $this->_view->renderizar('index');


    }


}