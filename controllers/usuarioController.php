<?php
/**
 * Created by JetBrains PhpStorm.
 * User: franciscogonzalez
 * Date: 1/18/13
 * Time: 12:37 PM
 * To change this template use File | Settings | File Templates.
 */
class usuarioController extends Controller
{
    private $_usuario;

    public function __construct() {
        parent::__construct();
        $this->_usuario = $this->loadModel('usuario');

    }
    private function upload_image($destination_dir,$name_media_field,$time){

		//print_r($destination_dir);
		
		//print_r($time);
        $filedata = $this->getFileParamMeters($name_media_field);
      	
		//si hemos enviado un directorio que existe realmente y hemos subido el archivo
        if ( is_dir($destination_dir) && is_uploaded_file($filedata['tmp_name']))
        {
            echo "aca1";
			$img_file  = $time.'-'.$filedata['img_file'];
            $img_type  = $filedata['img_type'];
            //echo 1;
            //¿es una imágen realmente?
            if (((strpos($img_type, "gif") || strpos($img_type, "jpeg") || strpos($img_type,"jpg")) || strpos($img_type,"png") )){
                //¿Tenemos permisos para subir la imágen?
                //echo 2;
				
                if(move_uploaded_file($filedata['tmp_name'], $destination_dir.$img_file)){
                    $this->img_file = $img_file;
                    return true;
                }
            }
        }
        //si llegamos hasta aquí es que algo ha fallado
        return false;
    }//end function


    public function index()
    {

        if(!Session::get('autenticado')){
            $this->redireccionar('error');
        }
        Session::acceso('admin');

        $this->_view->thispage = 'admusuario';
        $this->_view->usuario = $this->_usuario->getUsuarioById(Session::get('id_usuario'));
        $this->_view->renderizar('index');

    }




    public function lista($pagina = false)
    {
        if(!Session::get('autenticado')){
            $this->redireccionar('error');
        }
        $this->_view->thispage = 'admusuario';
        $this->_view->titulo = 'Listar Usuario';
        $this->_view->usuario = $this->_usuario->getUsuarioById(Session::get('id_usuario'));

        /* Paginador */
        if(!$this->filtrarInt($pagina)){
            $pagina = false;
        }
        else{
            $pagina = (int) $pagina;
        }
        $this->getLibrary('paginador');
        $paginador = new Paginador();
        $this->_view->usuarios = $paginador->paginar($this->_usuario->getUsuarios(), $pagina);
        $this->_view->paginacion = $paginador->getViewAdmin('paginador', 'usuario/lista');


        $this->_view->renderizar('lista');
    }
    public function crear(){

        if(!Session::get('autenticado')){
            $this->redireccionar('error');
        }
        Session::acceso('admin');
        $time = time();
        $this->_view->thispage = 'admusuario';
        $this->_view->titulo = 'Crear Usuario';
        $this->_view->usuario = $this->_usuario->getUsuarioById(Session::get('id_usuario'));


        if($this->getInt('enviar') == 1){
            $this->_view->datos = $_POST;
            if(!$this->getSql('nombre')){
                $this->_view->_error = 'Debe introducir su nombre';
                $this->_view->renderizar('crear');
                exit;
            }
            if(!$this->getAlphaNum('usuario')){
                $this->_view->_error = 'Debe introducir su nombre usuario';
                $this->_view->renderizar('crear');
                exit;
            }
            if($this->_usuario->verificarUsuario($this->getAlphaNum('usuario'))){
                $this->_view->_error = 'El usuario ' . $this->getAlphaNum('usuario') . ' ya existe';
                $this->_view->renderizar('crear');
                exit;
            }
            if(!$this->validarEmail($this->getPostParam('email'))){
                $this->_view->_error = 'La direccion de email es inv&aacute;lida';
                $this->_view->renderizar('crear');
                exit;
            }
            if($this->_usuario->verificarEmail($this->getPostParam('email'))){
                $this->_view->_error = 'Esta direccion de correo ya esta registrada';
                $this->_view->renderizar('crear');
                exit;
            }
            if(!$this->getSql('pass')){
                $this->_view->_error = 'Debe introducir su password';
                $this->_view->renderizar('crear');
                exit;
            }
            if($this->getPostParam('pass') != $this->getPostParam('confirmar')){
                $this->_view->_error = 'Los passwords no coinciden';
                $this->_view->renderizar('crear');
                exit;
            }
            if(!$this->upload_image($_SERVER['DOCUMENT_ROOT'] .'/clientes/views/layout/default/img/logos/','uploadImage',$time)){
                $this->_view->_error = 'la imagen no carga';
                $this->_view->renderizar('crear');
                exit;
            }

            if(!$this->getSql('ve_informe')){
                $ve_informe=0;
            }else{
                $ve_informe=1;
            }
            if(!$this->getSql('ve_paleta')){
                $ve_paleta=0;
            }else{
                $ve_paleta=1;
            }
            if(!$this->getSql('ve_orden')){
                $ve_orden=0;
            }else{
                $ve_orden=1;
            }
            if(!$this->getSql('ve_precio')){
                $ve_precio=0;
            }else{
                $ve_precio=1;
            }


            $registro = $this->_usuario->registrarUsuario(
                $this->getSql('nombre'),
                $this->getAlphaNum('usuario'),
                $this->getSql('pass'),
                $this->getPostParam('email'),
                $this->getSql('rol'),
                $this->img_file,
                $ve_informe,
                $ve_paleta,
                $ve_orden,
                $ve_precio
            );

            if($registro){
            $usuario = $this->_usuario->verificarUsuario($this->getAlphaNum('usuario'));
            if(!$usuario){
                $this->_view->_error = 'Error al registrar el usuario';
                $this->_view->renderizar('crear');
                exit;
             }
            }
            //$this->redireccionar('usuario');
            //$this->_view->datos = false;
            //$this->_view->_mensaje = 'Registro Completado';
        }


        $this->_view->renderizar('crear');
    }
    public function eliminar($id)
    {

        if(!Session::get('autenticado')){
            $this->redireccionar('error');
        }
        Session::acceso('admin');

        if(!$this->filtrarInt($id)){
            $this->redireccionar('usuario/lista');
        }

        if(!$this->_usuario->getUsuarioById($this->filtrarInt($id))){
            $this->redireccionar('usuario/lista');
        }

        $this->_usuario->eliminarUsuario($this->filtrarInt($id));
        $this->redireccionar('usuario/lista');
    }
    public function editar($id)
    {
        if(!Session::get('autenticado')){
            $this->redireccionar('error');
        }
        Session::acceso('admin');
        $time = time();
        $this->_view->thispage = 'admusuario';
        $this->_view->titulo = 'Editar Usuario';
        $this->_view->setJs(array('nuevo'));
        if(!$this->filtrarInt($id)){
            $this->redireccionar('lista');
        }
        if(!$this->_usuario->getUsuarioById($this->filtrarInt($id))){
            $this->redireccionar('lista');
        }
        $this->_view->usuario = $this->_usuario->getUsuarioById(Session::get('id_usuario'));
        $this->_view->datos = $this->_usuario->getUsuarioById($this->filtrarInt($id));
        if($this->getInt('guardar') == 1){
            if(!$this->getSql('nombre')){
                $this->_view->_error = 'Debe introducir su nombre';
                $this->_view->renderizar('editar');
                exit;
            }
            if(!$this->getAlphaNum('usuario')){
                $this->_view->_error = 'Debe introducir su nombre usuario';
                $this->_view->renderizar('editar');
                exit;
            }
            if(!$this->validarEmail($this->getPostParam('email'))){
                $this->_view->_error = 'La direccion de email es inv&aacute;lida';
                $this->_view->renderizar('editar');
                exit;
            }
			
			echo strlen($this->_view->datos['logo']);
            if(strlen($this->_view->datos['logo'])>0){
                if($this->getPostParam('eliminar')){
                    $this->img_file='';
                }else{
                    $this->img_file=$this->_view->datos['logo'];
                }
            }else{
				
                if(!$this->upload_image($_SERVER['DOCUMENT_ROOT'] .'/clientes/views/layout/default/img/logos/','uploadImage',$time)){
                    $this->_view->_error = 'la imagen no carga';
                    $this->_view->renderizar('editar');
                    //exit;
                }else{
					
					
				}
            
			}
            if(!$this->getSql('ve_informe')){
              $ve_informe=0;
            }else{
                $ve_informe=1;
            }
            if(!$this->getSql('ve_paleta')){
                $ve_paleta=0;
            }else{
                $ve_paleta=1;
            }
            if(!$this->getSql('ve_orden')){
                $ve_orden=0;
            }else{
                $ve_orden=1;
            }
            if(!$this->getSql('ve_precio')){
                $ve_precio=0;
            }else{
                $ve_precio=1;
            }
			
            $actualizar= $this->_usuario->editarUsuario($id,
                $this->getSql('nombre'),
                $this->getAlphaNum('usuario'),
                $this->getPostParam('email'),
                $this->getSql('rol'),
                $this->img_file,
                $ve_informe,
                $ve_paleta,
                $ve_orden,
                $ve_precio
            );
            if($actualizar){
            $this->_view->datos = $this->_usuario->getUsuarioById($this->filtrarInt($id));
            $this->_view->_mensaje = 'Registro Actualizado';
            $this->_view->renderizar('editar');
            }
        }
        $this->_view->renderizar('editar');
    }
    public function pass($id)
    {
        if(!Session::get('autenticado')){
            $this->redireccionar('error');
        }
        Session::acceso('admin');
        $this->_view->thispage = 'admusuario';
        $this->_view->titulo = 'Editar Password Usuario';
        if(!$this->filtrarInt($id)){
            $this->redireccionar('lista');
        }
        if(!$this->_usuario->getUsuarioById($this->filtrarInt($id))){
            $this->redireccionar('lista');
        }
        $this->_view->usuario = $this->_usuario->getUsuarioById(Session::get('id_usuario'));
        $this->_view->datos = $this->_usuario->getUsuarioById($this->filtrarInt($id));
        if($this->getInt('guardar') == 1){
            if(!$this->getSql('pass')){
                $this->_view->_error = 'Debe introducir su password';
                $this->_view->renderizar('pass');
                exit;
            }
            if($this->getPostParam('pass') != $this->getPostParam('confirmar')){
                $this->_view->_error = 'Los passwords no coinciden';
                $this->_view->renderizar('pass');
                exit;
            }
            $this->_usuario->editarUsuarioPass($id,$this->getSql('pass'));
            $this->_view->_mensaje = 'Password Actualizado';

        }
        $this->_view->renderizar('pass');
    }
}