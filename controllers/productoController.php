<?php
/*
 * Created by JetBrains PhpStorm.
 * User: franciscogonzalez
 * Date: 11/23/12
 * Time: 11:29 AM
 * To change this template use File | Settings | File Templates.
 *
*/

class productoController extends Controller
{

    private $_producto;
    private $_producto_usuario;

    private function validarRut($rut){

        $respuesta=0;

        foreach ($this->_view->rut_usuario as $ruLista){
            if($rut==$ruLista['rut']){
                $respuesta = 1;
                break;
            }else{ }
        }
        return $respuesta;
    }

    public function __construct() {
        parent::__construct();
        $this->_producto = $this->loadModel('producto');
        $this->_producto_usuario = $this->loadModel('usuario');
    }
    public function index($pagina = false)
    {
        Session::acceso('usuario');
        if(!Session::get('autenticado')){
            $this->redireccionar('index');
        }

        $this->getLibrary('paginador');
        $paginador = new Paginador();

        $this->_view->fecha=false;
        $this->_view->rut=false;
        $rut=false;
        $noRut=false;
        $this->_view->thispage = 'productos';

        $this->_view->avanzado=false;
        $this->_view->simple=true;
        $this->_view->tiene_fecha=false;
        $this->_view->tiene_rut=true;

        /* obtengo data de usuario*/
        $this->_view->usuario = $this->_producto_usuario->getUsuarioById(Session::get('id_usuario'));
        if($this->_view->usuario['ve_paleta']){


        $this->_view->rut_usuario_por_defecto = $this->_producto_usuario->getUsuarioRutPorDefecto($this->_view->usuario['id']);
        $this->_view->rut_usuario = $this->_producto_usuario->getUsuarioRuts($this->_view->usuario['id']);

        /* OBTENGO ULTIMA DATA SIN PETICION POST */
        if(!$this->getInt('enviar')){
            if(!empty($this->_view->rut_usuario_por_defecto)){
                $rut = $this->_view->rut_usuario_por_defecto['rut'];
                $this->_view->rut[]=$rut;
            }else{ }
        }
        /*Peticion Simple POST*/
        if($this->getInt('enviar') == 1){
            $this->_view->datos = $_POST;
            if(!empty($_POST['rut'])){
                $rut = $_POST['rut'];
                $this->_view->rut[]=$rut;
            }else{ }

        }

        if(count($rut)>1){
            foreach ($rut as $ru){
                $estado = $this->validarRut($ru);
                if($estado==0){
                    $noRut=true;
                }
            }
        }else{
            $estado = $this->validarRut($rut);
            if($estado==0){
                $noRut=true;
            }
        }
        if (!$noRut) {


            if($this->_producto->getArbolCategorias($rut)){
        $this->_view->arbol = $this->_producto->getArbolCategorias($rut);
        $this->_view->tipo = $this->_view->arbol[0][0];

        if($this->_producto->getProductSkuByCategory($rut,  $this->_view->arbol[0][0])){
            $this->_view->productosbycat =  $paginador->paginar($this->_producto->getProductSkuByCategory($rut,$this->_view->arbol[0][0]), $pagina);
        }

            }

        }
        $this->_view->base = BASE_URL;
        }else{
            $this->_view->mensaje_permisos="No tiene permisos para ver paleta";
        }
        $this->_view->renderizar('index');

    }


    public function productos($rut,$cat,$pagina=false)
    {
        Session::acceso('usuario');
        if(!Session::get('autenticado')){
            $this->redireccionar('index');
        }
       
        $this->_view->avanzado=false;
        $this->_view->simple=true;
        $this->_view->tiene_fecha=false;
        $this->_view->tiene_rut=true;

        $this->getLibrary('paginador');
        $paginador = new Paginador();
        $this->_view->fecha=false;
        $this->_view->rut=false;
        $noRut=false;
        $this->_view->thispage = 'productos';

        $cat = str_replace('-',' ',$cat);
        /* obtengo data de usuario*/
        $this->_view->usuario = $this->_producto_usuario->getUsuarioById(Session::get('id_usuario'));
        if($this->_view->usuario['ve_paleta']){
        $this->_view->rut_usuario_por_defecto = $this->_producto_usuario->getUsuarioRutPorDefecto($this->_view->usuario['id']);
        $this->_view->rut_usuario = $this->_producto_usuario->getUsuarioRuts($this->_view->usuario['id']);

        if(!$this->filtrarInt($pagina)){
            $pagina = false;
        }
        else{
            $pagina = (int) $pagina;
        }
        $this->_view->rut[]=$rut;
        $this->_view->tipo=$cat;

        if(count($rut)>1){
            foreach ($rut as $ru){
                $estado = $this->validarRut($ru);
                if($estado==0){
                    $noRut=true;
                }
            }
        }else{
            $estado = $this->validarRut($rut);
            if($estado==0){
                $noRut=true;
            }
        }
        if (!$noRut) {


        $this->_view->arbol = $this->_producto->getArbolCategorias($rut);

        if($this->_producto->getProductSkuByCategory($rut,$cat)){
        $this->_view->productosbycat =  $paginador->paginar($this->_producto->getProductSkuByCategory($rut,$cat), $pagina);

           // print_r($this->_view->productosbycat);
        }

            $cat = str_replace(' ','-',$cat);
        $this->_view->paginacion = $paginador->getView('paginador', 'producto/productos',$cat,$rut);
        }
        $this->_view->base = BASE_URL;
        }else{
            $this->_view->mensaje_permisos="No tiene permisos para ver paleta";
        }

        $this->_view->renderizar('producto');

    }

    public function ficha($rut,$id)
    {

        Session::acceso('usuario');
        if(!Session::get('autenticado')){
            $this->redireccionar('index');
        }
        $this->_view->avanzado=false;
        $this->_view->simple=true;
        $this->_view->tiene_fecha=false;
        $this->_view->tiene_rut=true;
        $this->_view->fecha=false;
        $this->_view->rut=false;
        $this->_view->thispage = 'ficha';
        $this->_view->rut[]=$rut;
        /* obtengo data de usuario*/
        $this->_view->usuario = $this->_producto_usuario->getUsuarioById(Session::get('id_usuario'));
        if($this->_view->usuario['ve_paleta']){

        $this->_view->ve_precio=$this->_view->usuario['ve_precio'];

        $this->_view->rut_usuario_por_defecto = $this->_producto_usuario->getUsuarioRutPorDefecto($this->_view->usuario['id']);
        $this->_view->rut_usuario = $this->_producto_usuario->getUsuarioRuts($this->_view->usuario['id']);

        if(!empty($rut) && !empty($id) ){

        $this->_view->arbol = $this->_producto->getArbolCategorias($rut);

        $this->_view->productobyid = $this->_producto->getProduct($id,$rut);

            //print_r($this->_view->productobyid);
        }
        $this->_view->base = BASE_URL;
        }else{
            $this->_view->mensaje_permisos="No tiene permisos para ver paleta";
        }

        $this->_view->renderizar('ficha');

    }



}