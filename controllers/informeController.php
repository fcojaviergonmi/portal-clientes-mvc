<?php
/*
 * Created by JetBrains PhpStorm.
 * User: franciscogonzalez
 * Date: 11/23/12
 * Time: 11:29 AM
 * To change this template use File | Settings | File Templates.
 *
 *
 */
class informeController extends Controller
{
    private $_informe;
    private $_informe_usuario;
    private $_fecha;

    private function validarRut($rut){

        $respuesta=0;

        foreach ($this->_view->rut_usuario as $ruLista){
            if($rut==$ruLista['rut']){
                $respuesta = 1;
                break;
            }else{ }
        }
        return $respuesta;
    }
    public function __construct() {
        parent::__construct();
        $this->_informe = $this->loadModel('informe');
        $this->_informe_usuario = $this->loadModel('usuario');
        $this->_fecha = $this->loadModel('fecha');
    }
    public function index($pagina = false)
    {
        /* declaro autenticacion */
        if(!Session::get('autenticado')){
            $this->redireccionar('index');
        }
        Session::acceso('usuario');
        /* inicio estado variables*/
        $fecha=false;
        $rut=false;
        $noRut=false;

        $this->_view->avanzado=true;
        $this->_view->simple=true;
        $this->_view->tiene_fecha=true;
        $this->_view->tiene_rut=true;

        $this->_view->fecha=false;
        $this->_view->rut=false;
        $this->_view->thispage = 'informe';
        /*agrego librerias*/
        $this->getLibrary('chart');
        /*definicion de caso*/
        $tipo_busqueda = "simple";
        /* obtengo data de usuario*/
        $this->_view->usuario = $this->_informe_usuario->getUsuarioById(Session::get('id_usuario'));
        if($this->_view->usuario['ve_informe']){
        /* FECHAS y RUTS */
        $this->_view->rut_usuario = $this->_informe_usuario->getUsuarioRuts($this->_view->usuario['id']);
        $this->_view->fechas_informe = $this->_fecha->getFechas();
        $this->_view->rut_usuario_por_defecto = $this->_informe_usuario->getUsuarioRutPorDefecto($this->_view->usuario['id']);


        /* OBTENGO ULTIMA DATA SIN PETICION POST */
        if(!$this->getInt('enviar')){

            if(!empty($this->_view->fechas_informe)){
                $fecha = $this->_view->fechas_informe[count($this->_view->fechas_informe)-1]['fecha'];
                $this->_view->fecha[]=$fecha;
            }else{ }
            if(!empty($this->_view->rut_usuario_por_defecto)){
                $rut = $this->_view->rut_usuario_por_defecto['rut'];
                $this->_view->rut[]=$rut;
            }else{ }
        }
        /*Peticion Simple POST*/
        if($this->getInt('enviar') == 1){
            $this->_view->datos = $_POST;
            if(!empty($_POST['fecha'])){
                $fecha = $_POST['fecha'];
                $this->_view->fecha[]=$fecha;
             }else{ }
            if(!empty($_POST['rut'])){
                $rut = $_POST['rut'];
                $this->_view->rut[]=$rut;
            }else{ }
            $tipo_busqueda = "simple";
        }
        /*Peticion Avanzada POST*/
        if($this->getInt('enviar') == 2){
            $this->_view->datos = $_POST;
            if(!empty($_POST['fechas'])){

                $fecha = $_POST['fechas'];
                $this->_view->fecha=$fecha;
            }else{ }
            if(!empty($_POST['ruts'])){

                $rut = $_POST['ruts'];
                $this->_view->rut=$rut;
            }else{ }
            $tipo_busqueda = "avanzado";
        }


        if($fecha && $rut){


            if(count($rut)>1){

                foreach ($rut as $ru){
                    $estado = $this->validarRut($ru);
                    if($estado==0){
                        $noRut=true;
                    }
                }
            }else{

                switch ($tipo_busqueda) {
                    case "simple":

                        $estado = $this->validarRut($rut);
                        break;
                    case "avanzado":


                        $estado = $this->validarRut($rut[0]);
                        break;
                }


                if($estado==0){
                    $noRut=true;
                }
            }

           


            if (!$noRut) {

                switch ($tipo_busqueda) {
                    case "simple":

                        $this->_view->informe = $this->_informe->getProductosMasConsumidos($rut, $fecha);
                        $this->_view->informe_por_region_nombres = $this->_informe->getProductosMasConsumidosPorSucursal($rut, $fecha);
                        $this->_view->informe_por_comunas = $this->_informe->getComunasProductos($rut, $fecha);
                        break;
                    case "avanzado":


                        $this->_view->informe = $this->_informe->getProductosMasConsumidosMulti($rut, $fecha);
                        $this->_view->informe_por_region_nombres = $this->_informe->getProductosMasConsumidosPorSucursalMulti($rut, $fecha);
                        $this->_view->informe_por_comunas = $this->_informe->getComunasProductosMulti($rut, $fecha);
                        break;
                }

                /* INICIO GRAFICO DE PIE*/
                $pie = array();
                $count_p = 0;
                /* lipiar para usar la API de GOOGLE*/
                foreach ($this->_view->informe as $categoria) {
                    $suma = str_replace(",", "", $categoria['vaneli']);
                    if (empty($suma)) {
                        $valor = 0;
                    } else {
                        $valor = $suma;
                    };
                    $pie[$count_p]['CATEGORIA'] = trim($categoria['nokofm']);
                    $pie[$count_p]['TOTAL'] = $valor;

                    $count_p++;
                }
                /* envio datos a la vista*/
                $this->_view->grafico_pie = $pie;
                $options = array('height' => 400, 'width' => 380, 'chartArea' => '{left:20,top:0,width:"100%",height:"100%"}');
                $options_bar = array('height' => 400, 'width' => 420);
                $this->_view->grafico_pie_opciones = $options;
                $this->_view->grafico_pie_opciones_bar = $options_bar;
                /* FIN GRAFICO DE PIE*/
                /* INICIO GRAFICO DE BARRAS*/
                $line = array();
                $count = 0;
                foreach ($this->_view->informe_por_comunas as $comunas) {
                    $line[$count]['comuna'] = trim($comunas['nokosu']);
                    foreach ($this->_view->informe_por_region_nombres as $key => $value) {
                        $suma = str_replace(",", "", $value[$comunas['nokosu']]);
                        if (empty($suma)) {
                            $precio = 0;
                        } else {
                            $precio = $suma;
                        };
                        $line[$count][trim($value['nokofm'])] = $precio;
                    }
                    $count++;
                }
                $this->_view->grafico_lineas = $line;
                $options = array('height' => 500, 'width' => 800, 'chartArea' => '{width:"60%",height:"70%"}');
                $this->_view->grafico_lineas_opciones = $options;
            }
        }
        }else{
            $this->_view->mensaje_permisos="No tiene permisos para ver informes";
        }
        $this->_view->renderizar('index');
    }
}