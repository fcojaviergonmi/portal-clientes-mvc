<?php
/**
 * Created by JetBrains PhpStorm.
 * User: franciscogonzalez
 * Date: 6/20/13
 * Time: 6:16 PM
 * To change this template use File | Settings | File Templates.
 */

class rutController extends Controller
{

    private $_usuario;
    private $_rut;

    public function __construct()
    {
        parent::__construct();
        $this->_usuario = $this->loadModel('usuario');
        $this->_rut = $this->loadModel('rut');
    }

    public function index()
    {
        if (!Session::get('autenticado')) {
            $this->redireccionar('error');
        }
        Session::acceso('admin');
        $this->_view->titulo = 'ADMINISTRADOR DE RUT';
        $this->_view->thispage = 'admrut';
        $this->_view->usuario = $this->_usuario->getUsuarioById(Session::get('id_usuario'));
        $this->_view->renderizar('index');
    }

    public function crear()
    {
        if (!Session::get('autenticado')) {
            $this->redireccionar('error');
        }
        Session::acceso('admin');

        $this->_view->thispage = 'admrut';
        $this->_view->titulo = 'CREAR RUT';
        $this->_view->usuario = $this->_usuario->getUsuarioById(Session::get('id_usuario'));

        if($this->getInt('enviar') == 1){

            if(!$this->getSql('rut')){
                $this->_view->_error = 'Debe introducir un rut';
                $this->_view->renderizar('crear');
                exit;
            }
            if($this->_rut->verificarRut($this->getSql('rut'))){
                $this->_view->_error = 'El rut ' . $this->getSql('rut') . ' ya existe';
                $this->_view->renderizar('crear');
                exit;
            }
            if(strlen($this->getSql('rut')) < 8){

            $this->_view->_error = 'El rut ' . $this->getSql('rut') . ' es muy corto';
            $this->_view->renderizar('crear');
            exit;
            }
            if(strpos($this->getSql('rut'), ",")){

                $this->_view->_error = 'El rut ' . $this->getSql('rut') . ' no puede tener comas';
                $this->_view->renderizar('crear');
                exit;
            }

            if(strpos($this->getSql('rut'), ".")){

                $this->_view->_error = 'El rut ' . $this->getSql('rut') . ' no puede tener puntos';
                $this->_view->renderizar('crear');
                exit;
            }
            $registro = $this->_rut->registrarRut(
                $this->getSql('rut')
            );
            if($registro){
                $rut= $this->_rut->verificarRut($this->getSql('rut'));
                if(!$rut){
                    $this->_view->_error = 'Error al registrar el rut';
                    $this->_view->renderizar('crear');
                    exit;
                }
            }
            $this->redireccionar('rut');
            $this->_view->datos = false;
            $this->_view->_mensaje = 'Registro Completado';

        }
        $this->_view->renderizar('crear');
    }
    public function lista($pagina = false)
    {
        if(!Session::get('autenticado')){
            $this->redireccionar('error');
        }
        $this->_view->thispage = 'admrut';
        $this->_view->titulo = 'LISTA DE RUT';
        $this->_view->usuario = $this->_usuario->getUsuarioById(Session::get('id_usuario'));

        if(!$this->filtrarInt($pagina)){
            $pagina = false;
        }
        else{
            $pagina = (int) $pagina;
        }
        $this->getLibrary('paginador');
        $paginador = new Paginador();


        $this->_view->ruts = $paginador->paginar($this->_rut->getRUTS(), $pagina);
        $this->_view->paginacion = $paginador->getViewAdmin('paginador', 'rut/lista');

        $this->_view->renderizar('lista');
    }
    public function eliminar($id)
    {

        if(!Session::get('autenticado')){
            $this->redireccionar('error');
        }
        Session::acceso('admin');

        if(!$this->filtrarInt($id)){
            $this->redireccionar('rut/lista');
        }

        if(!$this->_rut->getRutById($this->filtrarInt($id))){
            $this->redireccionar('rut/lista');
        }

        $this->_rut->eliminarRut($this->filtrarInt($id));
        $this->redireccionar('rut/lista');
    }
    public function agregar($id = false,$accion= false,$id_usuario= false)
    {
        if(!Session::get('autenticado')){
            $this->redireccionar('error');
        }
        Session::acceso('admin');
        $this->_view->thispage = 'admrut';
        $this->_view->titulo = 'AGREGAR USUARIOS';
        $this->_view->usuario = $this->_usuario->getUsuarioById(Session::get('id_usuario'));

        if(!$this->filtrarInt($id)){
            $this->redireccionar('rut/lista');
        }
        if(!$this->_rut->getRutById($this->filtrarInt($id))){
            $this->redireccionar('rut/lista');
        }

        if($accion=='eliminar'){

           $this->_rut->eliminarUsuarioById($this->filtrarInt($id),$this->filtrarInt($id_usuario));
            $this->redireccionar('rut/agregar/'.$id);
        }
        if($this->getInt('enviar') == 1){
            if($this->getSql('usuarios')==1){
            }else{
                $verficar=$this->_rut->validarUsuario($id,$this->getSql('usuarios'));

                if(!$verficar){

                    $insert=$this->_rut->agregarUsuario($id,$this->getSql('usuarios'));
                    if($insert){
                        $this->_view->_mensaje = 'Usuario Agregado';
                    }
                }
            }
        }
        $this->_view->rut=$this->_rut->getRutById($this->filtrarInt($id));
        $this->_view->rut_usuarios = $this->_usuario->getUsuarioByIdRut($this->filtrarInt($id));
        $this->_view->rut_usuarios_disponibles = $this->_rut->getUsuarioDisponibles($this->filtrarInt($id));

        $this->_view->renderizar('agregar');
    }

}