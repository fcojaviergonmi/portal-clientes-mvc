<?php
/**
 * Created by JetBrains PhpStorm.
 * User: franciscogonzalez
 * Date: 6/20/13
 * Time: 6:16 PM
 * To change this template use File | Settings | File Templates.
 */
class cargarordenesController extends Controller
{
    private $_usuario;
    private $_cargar;
    private $_fecha;

    public function __construct()
    {
        parent::__construct();
        $this->_usuario = $this->loadModel('usuario');
        $this->_cargar = $this->loadModel('cargar');
        $this->_fecha = $this->loadModel('fecha');
    }

    public function index($pagina=false)
    {
        if (!Session::get('autenticado')) {
            $this->redireccionar('error');
        }
        Session::acceso('admin');
        $this->_view->thispage = 'admcargarordenes';

        $this->_view->usuario = $this->_usuario->getUsuarioById(Session::get('id_usuario'));
        /* Paginador */
        if(!$this->filtrarInt($pagina)){
            $pagina = false;
        }
        else{
            $pagina = (int) $pagina;
        }

        $this->getLibrary('paginador');
        $paginador = new Paginador();
        $this->_view->historico = $paginador->paginar($this->_cargar->getCargasHistoricas(2), $pagina);
        $this->_view->paginacion = $paginador->getViewAdmin('paginador', 'cargarordenes/index');

        $this->_view->renderizar('index');
    }

    public function informe($pagina=false)
    {
        if (!Session::get('autenticado')) {
            $this->redireccionar('error');
        }
        Session::acceso('admin');
        $this->_view->thispage = 'admcargarordenes';

        $this->_view->usuario = $this->_usuario->getUsuarioById(Session::get('id_usuario'));

        /* Paginador */
        if(!$this->filtrarInt($pagina)){
            $pagina = false;
        }
        else{
            $pagina = (int) $pagina;
        }
        $this->getLibrary('paginador');
        $paginador = new Paginador();
        $this->_view->historico = $paginador->paginar($this->_cargar->getCargasHistoricas(2), $pagina);
        $this->_view->paginacion = $paginador->getViewAdmin('paginador', 'cargarordenes/index');

        if($this->getSql('MAX_FILE_SIZE')){

            set_time_limit(0);
            ini_set('display_errors', 1);
            ini_set('memory_limit', '128M');
            error_reporting(E_ALL);




            $this->getLibraryPath('Classes');
            include 'PHPExcel/IOFactory.php';

            $inputFileName = $_FILES['file']['tmp_name'];
            $inputFileRealName = $_FILES["file"]["name"];
            if ($_FILES['file']['tmp_name']) {
                $this->_view->_mensaje = 'Loading file '. pathinfo($inputFileName, PATHINFO_BASENAME). ' using IOFactory to identify the format<br />';
                try {
                    $objPHPExcel = PHPExcel_IOFactory::load($inputFileName);
                } catch (PHPExcel_Reader_Exception $e) {
                    die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME) . '": ' . $e->getMessage());
                    $this->_view->renderizar('index');
                }

                $validColumns = array(0, 1, 2, 3, 4,5,6,7);

                $sheetData = $objPHPExcel->getActiveSheet();



                $highestRow = $sheetData->getHighestRow(); // e.g. 10
                $highestColumn = $sheetData->getHighestColumn(); // e.g 'F'
                $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);

                $fecha_carga = date('Y-m-d H:i:s');
                $sql_data = "";
                $len = count($validColumns);


                $cellnudo= $sheetData->getCellByColumnAndRow(0, 1);
                $cellendo = $sheetData->getCellByColumnAndRow(1, 1);
                $cellsucentidad = $sheetData->getCellByColumnAndRow(2, 1);
                $cellordencomp = $sheetData->getCellByColumnAndRow(3, 1);
                $cellemision= $sheetData->getCellByColumnAndRow(4, 1);
                $cellestado = $sheetData->getCellByColumnAndRow(5, 1);
                $celltotal= $sheetData->getCellByColumnAndRow(6, 1);
                $celltotald= $sheetData->getCellByColumnAndRow(7, 1);

                if($cellnudo=='NUDO' && $cellendo=='ENDO' && $cellsucentidad=='SUCENTIDAD'
                    && $cellordencomp=='ORDENCOMP' && $cellemision=='EMISION' && $cellestado=='ESTADO'
                    && $celltotal=='TOTAL'&& $celltotald=='TOTALD'){

                    for ($row = 2; $row <= $highestRow; ++$row) {
                        $sql_data .= "(";
                        $i = 0;
                        for ($j =0; $j<$len; $j++) {

                            if ($i == $len - 1) {
                                if ($validColumns[$j] == 4) {
                                    $cell = $sheetData->getCellByColumnAndRow($validColumns[$j] , $row);
                                    $val = $cell->getValue();
                                    $date = PHPExcel_Style_NumberFormat::toFormattedString($val, 'YYYY-MM-DD');
                                    $sql_data .= "'" . $date . "'";
                                } else {
                                    $cell = $sheetData->getCellByColumnAndRow($validColumns[$j] , $row);
                                    $val = $cell->getValue();
                                    $sql_data .= "'" . trim($val) . "'";
                                }
                            } else {
                                if ($validColumns[$j]  == 4) {
                                    $cell = $sheetData->getCellByColumnAndRow($validColumns[$j] , $row);
                                    $val = $cell->getValue();
                                    $date = PHPExcel_Style_NumberFormat::toFormattedString($val, 'YYYY-MM-DD');
                                    $sql_data .= "'" . $date . "',";
                                } else {
                                    $cell = $sheetData->getCellByColumnAndRow($validColumns[$j] , $row);
                                    $val = $cell->getValue();
                                    $sql_data .= "'" . trim($val) . "',";
                                }
                            }
                            $i++;
                        }
                        if ($row == $highestRow) {
                            $sql_data .= ",'$fecha_carga')";
                        } else {
                            $sql_data .= ",'$fecha_carga'),";
                        }
                    }

                    $sql = 'INSERT INTO '.DB_PREFIX.'orden ( `nudo`, `endo`, `sucentidad`, `ordencomp`, `emision`, `estado`, `total`, `totald`, `fecha_carga`) VALUES';
                    $sql .= $sql_data;


                    if ($this->_cargar->insertInformeOrdenes($sql)) {

                            $this->_view->_mensaje = 'Carga completa';

                            $this->_cargar->insertRegistroHistorico(2,$fecha_carga,$inputFileRealName);
                            $this->redireccionar('cargarordenes/index');

                    }else{
                        $this->_view->_mensaje = 'Problema con el archivo';
                    }

                }else{
                    $this->_view->_mensaje = 'Problema con el archivo';
                }
            }
        }
        $this->_view->renderizar('index');
    }

    public function eliminar($id)
    {
        if(!Session::get('autenticado')){
            $this->redireccionar('error');
        }
        Session::acceso('admin');

        if(!$this->filtrarInt($id)){
            $this->redireccionar('cargarordenes/index');
        }

        $this->_cargar->eliminarCargaOrdenes($this->filtrarInt($id));
        $this->redireccionar('cargarordenes/index');
    }


}