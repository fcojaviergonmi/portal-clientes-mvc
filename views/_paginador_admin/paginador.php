<?php if(isset($this->_paginacion)): ?>
<div class="paginador">
<?php if($this->_paginacion['primero']): ?>
	
	<a href="<?php echo $link. $this->_paginacion['primero'].'/'; ?>">PRIMERO</a>
	
<?php else: ?>
	
	PRIMERO

<?php endif; ?>

&nbsp;

<?php if($this->_paginacion['anterior']): ?>
	
	<a href="<?php echo $link. $this->_paginacion['anterior'].'/'; ?>">ANTERIOR</a>
	
<?php else: ?>
	
	ANTERIOR

<?php endif; ?>

&nbsp;

<?php for($i = 0; $i < count($this->_paginacion['rango']); $i++): ?>
	
	<?php if($this->_paginacion['actual'] == $this->_paginacion['rango'][$i]): ?>
	
		<?php echo $this->_paginacion['rango'][$i]; ?>
	
	<?php else: ?>
		
		<a href="<?php echo $link.$this->_paginacion['rango'][$i].'/'; ?>">
			<?php echo $this->_paginacion['rango'][$i]; ?>
		</a>&nbsp;
	
	<?php endif; ?>
	
<?php endfor; ?>


&nbsp;

<?php if($this->_paginacion['siguiente']): ?>
	
	<a href="<?php echo $link. $this->_paginacion['siguiente'].'/'; ?>">SIGUIENTE</a>
	
<?php else: ?>

    SIGUIENTE

<?php endif; ?>

&nbsp;

<?php if($this->_paginacion['ultimo']): ?>
	
	<a href="<?php echo $link. $this->_paginacion['ultimo'].'/'; ?>">ULTIMO</a>
	
<?php else: ?>

    ULTIMO

<?php endif; ?>
	</div>
<?php endif; ?>