<!doctype html>
<html lang="es">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <?php $thispage = $this->thispage ?>
    <script type="text/javascript"> var $thispage = 'index'; </script>
    <title><?php if(isset($this->titulo)){ echo $this->titulo; } ?></title>
    <link type="image/x-icon" rel="shortcut icon" href="<?php echo $_layoutParams['ruta_js']; ?>favicon.ico"/>
    <!-- css -->
    <link type="text/css" rel="stylesheet" media="screen" href="<?php echo $_layoutParams['ruta_css']; ?>reset.css"/>
    <link type="text/css" rel="stylesheet" media="screen" href="<?php echo $_layoutParams['ruta_css']; ?>default.css"/>
    <link type="text/css" rel="stylesheet" media="screen" href="<?= BASE_URL ?>views/layout/default/font/font.css"/>
    <link type="text/css" rel="stylesheet" media="screen" href="<?php echo $_layoutParams['ruta_css']; ?>print.css"/>
    <link type="text/css" rel="stylesheet" media="screen" href="<?php echo $_layoutParams['ruta_css']; ?>style.css"/>
    <!--[if IE]>
    <script src="<?php echo $_layoutParams['ruta_js']; ?>html5.js"></script>
    <![endif]-->
    <!--[if IE 7]>
    <link type="text/css" rel="stylesheet" media="screen"
          href="<?php echo $_layoutParams['ruta_css']; ?>style7.css?=<?php echo time();?>"/>
    <![endif]-->
    <?php $isiPad = (bool)strpos($_SERVER['HTTP_USER_AGENT'], 'iPad');
    if (!empty($isiPad)) { //detecta si el navegador es iPad
        ?>
        <link type="text/css" rel="stylesheet" media="screen"
              href="<?php echo $_layoutParams['ruta_css']; ?>ipad.css?=<?php echo time(); ?>"/>
    <?php } ?>
    <!-- /css -->
    <!-- jQuery -->
    <script type="text/javascript" src="<?php echo $_layoutParams['ruta_js']; ?>jquery-1.8.3.min.js"></script>
    <!-- /jQuery -->
    <?php include('header_elements.php'); ?>
    <script src="<?php echo $_layoutParams['ruta_js']; ?>scripts.js" type="text/javascript"></script>
    <?php include('analytics.php'); ?>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#avanzada').click(function () {
                $(".mod-simple").hide();
                $(".mod-avanzado").show();
                $("#avanzada").attr("class", "seleccionado");
                $("#simple").attr("class", "no-seleccionado");

            });
            <?php if ($this->thispage == 'informe'): ?>
            $('#simple').click(function () {
                $(".mod-simple").show();
                $(".mod-avanzado").hide();
                $("#avanzada").attr("class", "no-seleccionado");
                $("#simple").attr("class", "seleccionado");

            });
            <?php endif; ?>
            $('.close').click(function () {

                $("#error").hide();
                $("#mensaje").hide();


            });
        });
    </script>
	
	<script type="text/javascript">
	var _gaq = _gaq || [];
	_gaq.push(['_setAccount', 'UA-21628016-3']);
	// Recommanded value by Google doc and has to before the trackPageView
	_gaq.push(['_setSiteSpeedSampleRate', 5]);

	_gaq.push(['_trackPageview']);


	(function() {
		var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
		ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
		var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	})(); 
	</script>
</head>
<body id="index">
<header>
    <div class="col">
        <figure id="logo-garmendia"><a href="http://www.garmendia.cl"><img
                    src="<?php echo $_layoutParams['ruta_img']; ?>garmendia.png" width="237" border="0" height="62"
                    alt="Garmendia"></a></figure>
        <?php if (Session::get('autenticado')) { ?>
            <div class="btn-cerrar">
                <a href="<?php echo BASE_URL; ?>login/cerrar"><img
                        src="<?php echo $_layoutParams['ruta_img']; ?>cerrar-sesion.png" width="98" height="17"
                        alt="Cerrar Sesi&oacute;n"></a>
            </div>
            <div class="clear"></div>
        <?php } ?>
        <?php
        if (Session::get('autenticado')) {
            if (Session::get('level') == 3) {
                ?>
                <nav class="menu">
                    <a class="pestana <?php if ($thispage == 'admusuario') {echo 'current';} ?>" href="<?= BASE_URL ?>usuario">ADM USUARIOS</a>
                    <a class="pestana <?php if ($thispage == 'admcargar') {echo 'current';} ?>" href="<?= BASE_URL ?>cargar">ADM INFORMES</a>
                    <a class="pestana <?php if ($thispage == 'admcargarordenes') {echo 'current';} ?>" href="<?= BASE_URL ?>cargarordenes">ADM ORDENES</a>
                    <a class="pestana <?php if ($thispage == 'admcargarpaleta') {echo 'current';} ?>" href="<?= BASE_URL ?>cargarpaleta">ADM PALETA</a>
                    <a class="pestana <?php if ($thispage == 'admrut') {echo 'current';} ?>" href="<?= BASE_URL ?>rut">ADM RUT</a>
                    <a class="pestana <?php if ($thispage == 'admfecha') {echo 'current';} ?>" href="<?= BASE_URL ?>fecha">ADM FECHA</a>
                    <div class="clear"></div>
                </nav>
                <div class="bienvenido">
                    <ul>
                        <li>
						<?= $this->usuario['nombre'] ?>
                    
                        </li>
                    </ul>
                    <div class="clear"></div>
                </div>
                <div id="foto_usuario">
                
                    <?php
                    //TODO: Cambiar sistema de logos.
                    //if (file_exists($_SERVER['DOCUMENT_ROOT'] . '/clientes2/views/layout/default/img/' . 'logos/' . $this->usuario['logo'])) {
                    if(strlen($this->usuario['logo']>0)){
					    echo '<img src="' . $_layoutParams['ruta_img'] . 'logos/' . $this->usuario['logo'] . '" border="0" alt="imagen usuario">';
                    } else { }
                    ?>
                </div>
            <?php } else { ?>
                <div id="foto_usuario">
                    <?php
                    //TODO: Cambiar sistema de logos.
					
                    //if (file_exists($_SERVER['DOCUMENT_ROOT'] . '/clientes2/views/layout/default/img/' . 'logos/' . $this->usuario['logo'])) {
				
					if(strlen($this->usuario['logo']>0)){
                        echo '<img src="' . $_layoutParams['ruta_img'] . 'logos/' . $this->usuario['logo'] . '" border="0" alt="imagen usuario">';
                    } else { }
                    ?>
                </div>
                <nav class="menu">
                    <a class="pestana <?php if ($thispage == 'productos') {
                        echo 'current';
                    } ?>" href="<?= BASE_URL ?>producto">PRODUCTOS</a>
                    <a class="pestana <?php if ($thispage == 'status') {
                        echo 'current';
                    } ?>" href="<?= BASE_URL ?>orden">STATUS DE ORDEN</a>
                    <a class="pestana <?php if ($thispage == 'informe') {
                        echo 'current';
                    } ?>" href="<?= BASE_URL ?>informe">INFORME MENSUAL</a>
                    <div class="clear"></div>
                </nav>
                <div class="bienvenido">
                    <ul>
                        <li><?= $this->usuario['nombre'] ?></li>
                    </ul>
                    <div class="clear"></div>
                </div>
            <?php } ?>
        <?php } ?>
        <div class="clear"></div>
        <div id="titulo">
            <span class="text-chico">PORTAL DE</span>
            <span class="text-grande">CLIENTES CORPORATIVOS</span>
        </div>
    </div>
    <div class="clear"></div>
</header>
<?php
if (Session::get('autenticado')) {
    if (Session::get('level') == 1) {
        ?>
        <div class="new-busq">
            <div class="cont-form-busq">
                <div class="selector">
                    <ul>
                        <li><a class="seleccionado" href="#" id="simple">B&Uacute;SQUEDA SIMPLE</a></li>
                        <?php if ($this->thispage == 'informe'): ?>
                            <li><a href="#" id="avanzada">B&Uacute;SQUEDA AVANZADA</a></li>
                        <?php endif; ?>
                        <div class="clear"></div>
                    </ul>
                    <div class="clear"></div>
                </div>
                <div class="form-cont">
                    <div class="mod-simple" style="display:block">
                        <?php if ($this->thispage == 'productos' || $this->thispage == 'ficha'){ ?>
                        <form id="simple" name="simple" action="<?=$this->base?>producto/" method="POST">
                            <?php }else{ ?>
                            <form id="simple" name="simple" action="" method="POST">
                                <?php }; ?>
                                <input type="hidden" value="1" name="enviar"/>
                                <ul>
                                    <?php if (isset($this->rut_usuario) && count($this->rut_usuario)) : ?>
                                        <li><label>SELECCIONAR RUT</label>

                                            <select id="rut" name="rut">
                                                <option value="0" selected="selected">SELECCIONAR RUT</option>
                                                <?php
                                                foreach ($this->rut_usuario as $rut) {
                                                    if (!empty($rut['rut'])) {
                                                        echo '<option value="' . $rut['rut'] . '"     >' . $rut['rut'] . '</option>';
                                                    }
                                                }
                                                ?>
                                            </select>

                                        </li>
                                    <?php endif; ?>
                                    <?php if ($this->tiene_fecha): ?>

                                        <?php if (isset($this->fechas_informe) && count($this->fechas_informe)) : ?>
                                            <li><label>SELECCIONAR FECHA</label>
                                                <select id="fecha" name="fecha">
                                                    <option value="0" selected="selected">SELECCIONAR FECHA</option>
                                                    <?php
                                                    foreach ($this->fechas_informe as $fecha) {
                                                        echo '<option value="' . $fecha['fecha'] . '"     >' . $fecha['fecha'] . '</option>';
                                                    }
                                                    ?>
                                                </select>
                                            </li>
                                        <?php endif; ?>

                                    <?php endif; ?>
                                    <div class="clear"></div>
                                </ul>
                                <div class="btn_buscar"><input name="Buscar" type="submit" id="Buscar" value="BUSCAR"></div>
                                <div class="clear"></div>
                            </form>
                    </div>
                    <?php if ($this->avanzado): ?>
                        <div class="mod-avanzado" style="display:none">
                            <form id="avanzado" name="avanzado" action="" method="POST">
                                <input type="hidden" value="2" name="enviar"/>

                                <div class="col-ruts">
                                    <?php if (isset($this->rut_usuario) && count($this->rut_usuario)) : ?>
                                        <label>SELECCIONE RUT:</label>
                                        <ul class="panel-selec">
                                            <?php
                                            foreach ($this->rut_usuario as $ruts) {
                                                if (!empty($ruts['rut'])) {
                                                    echo '<label><input type="checkbox" name="ruts[]" value="' . $ruts['rut'] . '">' . $ruts['rut'] . '</label>';
                                                }
                                            }
                                            ?>
                                            <div class="clear"></div>
                                        </ul>
                                        <div class="clear"></div>
                                    <?php endif; ?>
                                </div>
                                <div class="col-fechas">
                                    <?php if (isset($this->fechas_informe) && count($this->fechas_informe)) : ?>
                                        <label>SELECCIONE FECHAS:</label>
                                        <ul class="panel-selec">

                                            <?php
                                            foreach ($this->fechas_informe as $fecha) {

                                                echo '<label><input type="checkbox" name="fechas[]" value="' . $fecha['fecha'] . '">' . $fecha['fecha'] . '</label>';

                                            }
                                            ?>

                                            <div class="clear"></div>
                                        </ul>
                                        <div class="clear"></div>
                                    <?php endif; ?>
                                </div>
                                <div class="clear"></div>
                                <div class="btn_buscar"><input name="Buscar" type="submit" id="Buscar" value="BUSCAR">
                                </div>
                            </form>
                        </div>
                    <?php endif; ?>

                    <?php if (isset($this->mensaje_permisos)): ?>
                        <div class="resultados">
                            <h4><?=$this->mensaje_permisos?></h4>
                        </div>
                    <?php endif; ?>
                    <?php if (!empty($this->rut) || !empty($this->fecha)): ?>
                        <div class="resultados">
                            <h3>B&Uacute;SQUEDA REALIZADA</h3>
                            <ul class="res-cont">
                                <?php if ($this->tiene_rut): ?>
                                    <ul class="colres colder">
                                        <h4>RUT</h4>
                                        <?php
                                        if (count($this->rut) > 1) {
                                            foreach ($this->rut as $ru) {
                                                echo '<li>' . $ru . '</li>';
                                            }
                                        } else {
                                            echo '<li>' . $this->rut[0] . '</li>';
                                        }
                                        ?>
                                        <div class="clear"></div>
                                    </ul>
                                <?php endif; ?>
                                <?php if ($this->tiene_fecha): ?>
                                    <ul class="colres colizq">
                                        <h4>FECHA</h4>
                                        <?php
                                        if (count($this->fecha) > 1) {
                                            foreach ($this->fecha as $fe) {
                                                echo '<li>' . $fe . '</li>';
                                            }
                                        } else {
                                            echo '<li>' . $this->fecha[0] . '</li>';
                                        }
                                        ?>
                                        <div class="clear"></div>
                                    </ul>
                                <?php endif; ?>
                                <div class="clear"></div>
                            </ul>
                            <div class="clear"></div>
                        </div>
                    <?php endif; ?>
                    <div class="clear"></div>
                </div>
                <div class="clear"></div>
            </div>
            <div class="clear"></div>
        </div>
    <?php } ?>
<?php } ?>
<noscript><p>Para el correcto funcionamiento debe tener el soporte de javascript habilitado</p></noscript>
<?php if (isset($this->_error)): ?>
    <div id="error">
        <a href="#" class="close"><div class="cerrar"></div></a>
        <?php echo $this->_error; ?></div>
<?php endif; ?>
<?php if (isset($this->_mensaje)): ?>
<div id="mensaje">
    <a href="#" class="close"><div class="cerrar"></div></a>
    <?php echo $this->_mensaje; ?>
</div>
<?php endif; ?>