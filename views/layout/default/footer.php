<footer>
    <div class="col">
        <figure class="logo-footer"><img src="<?php echo $_layoutParams['ruta_img']; ?>garmedia_footer.png" width="145" height="34" alt="Garmendia"></figure>
        <div class="direccion">
          <p>Carlos Fern&aacute;ndez 255, San Joaqu&iacute;n, Santiago<br/>

              Tel&eacute;fono +(56 2) 600 426 7000 Fax +(56 2) 2422 9506</p>
            <div class="clear"></div>
        </div>
        <div class="equipo">
            <h3>EQUIPO CORPORATIVO</h3>
            <p>JORGE CAYUQUEO - JEFE DE CUENTAS CORPORATIVAS<br/>
                Correo: corporativo2@garmendia.cl<br/>

                Fono fijo: +(56 2) 2 422 95 37 / Fono m&oacute;vil: 7 775 4945</p>
            <div class="clear"></div>
        </div>
        <div class="clear"></div>
    </div>
    <div class="clear"></div>
</footer>
</body>
</html>
