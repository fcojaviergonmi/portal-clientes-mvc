<?php
/**
 * Created by JetBrains PhpStorm.
 * User: franciscogonzalez
 * Date: 11/21/12
 * Time: 9:58 AM
 * To change this template use File | Settings | File Templates.
 *
 *
 */
class Database extends PDO
{
    public function __construct()
    {
        parent::__construct(
            'mysql:host=' . DB_HOST .
            ';dbname=' . DB_NAME,
            DB_USER,
            DB_PASS,
            array(
                PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES ' . DB_CHAR
            ));
    }
}
?>
