<?php

/**
 * Created by JetBrains PhpStorm.
 * User: franciscogonzalez
 * Date: 11/21/12
 * Time: 9:58 AM
 * To change this template use File | Settings | File Templates.
 *
 *
 */
class Request
{
    private $_controlador;
    private $_metodo;
    private $_argumentos;

    public function __construct() {

        /* Valido URL */
        if(isset($_GET['url'])){
            /* Obtengo URL */
            $url = filter_input(INPUT_GET, 'url', FILTER_SANITIZE_URL);
            /* Convierto en ARRAY */
            $url = explode('/', $url);
            /* Elimino excesos de / */
            $url = array_filter($url);
            /* Asiganamos valores a los atributos */
            /* Primer elemento del array "CONTROLLER"*/
            $this->_controlador = strtolower(array_shift($url));
            /* Segundo elemento del array "METODO"*/
            $this->_metodo = strtolower(array_shift($url));
            $this->_argumentos = $url;

        }

        /* Validamos que existan */

        /* si no existe controlador, derivamos a controlador index */
        if(!$this->_controlador){
            $this->_controlador = DEFAULT_CONTROLLER;
        }
        /* si no existe metodo, derivamos a metodo index */
        if(!$this->_metodo){
            $this->_metodo = 'index';
        }
        /* si no existe argumento, asignamos array sin datos */
        if(!isset($this->_argumentos)){
            $this->_argumentos = array();
        }
    }
    
    public function getControlador()
    {
        return $this->_controlador;
    }
    
    public function getMetodo()
    {
        return $this->_metodo;
    }
    
    public function getArgs()
    {
        return $this->_argumentos;
    }
}

?>