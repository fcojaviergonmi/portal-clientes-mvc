<?php
/**
 * Created by JetBrains PhpStorm.
 * User: franciscogonzalez
 * Date: 11/21/12
 * Time: 9:58 AM
 * To change this template use File | Settings | File Templates.
 *
 *
 */
class Hash
{
    public static function getHash($algoritmo, $data, $key)
    {
        $hash = hash_init($algoritmo, HASH_HMAC, $key);
        hash_update($hash, $data);
        
        return hash_final($hash);
    }
}

?>
